from numpy.typing import NDArray
import numpy as np

Vector = NDArray[np.float64]
