from __future__ import annotations
from pathlib import Path
from typing import TextIO, Iterator
from src import Vector
import itertools as it
import numpy as np

# from https://nssdc.gsfc.nasa.gov/planetary/factsheet/
MASS = {  # kg
    "mercury": 0.330e24,
    "venus": 4.87e24,
    "earth": 5.97e24,
    "mars": 0.642e24,
    "jupiter": 1898e24,
    "saturn": 568e24,
    "uranus": 86.8e24,
    "neptune": 102e24,
    "sun": 1.98847e30,
}


class PositionData:
    PATH = Path("data")

    CSV_START = "$$SOE"
    CSV_END = "$$EOE"

    @staticmethod
    def parse_line(line: str) -> tuple[float, Vector, Vector]:
        """returns a tuple (time, pos, vel), where time is a unix timestamp, and pos and vel are numpy array"""
        # a line looks like this:
        #             JDTDB,            Calendar Date (TDB),                      X,                      Y,                      Z,                     VX,                     VY,                     VZ,             X_s,             Y_s,             Z_s,            VX_s,            VY_s,            VZ_s,
        # 2455193.500000000, A.D. 2009-Dec-28 00:00:00.0000, -1.104770748549123E-01,  9.805472252458164E-01, -2.023430355091084E-05, -1.737997923134082E-02, -1.942166496712403E-03,  4.813869811169272E-07,            n.a.,            n.a.,            n.a.,            n.a.,            n.a.,            n.a.,
        time, _, *nums = line.split(",")

        time = float(time)
        x, y, z, vx, vy, vz = [float(n.strip()) for n in nums[:6]]
        pos = np.array((x, y, z))
        vel = np.array((vx, vy, vz))

        return (time, pos, vel)

    def __init__(self, name: str) -> None:
        file: TextIO | None = None
        for data in PositionData.PATH.iterdir():
            if data.is_file() and name == data.stem:
                file = data.open()
                break

        if file is None:
            raise ValueError(f"Celestial {name} not in dataset")
        self.file: TextIO = file

        lines: Iterator[str] = self.file
        lines = map(lambda line: line.strip(), lines)
        lines = it.dropwhile(lambda line: line != PositionData.CSV_START, lines)
        lines = it.islice(lines, 1, None)  # skip CSV_START line
        lines = it.takewhile(lambda line: line != PositionData.CSV_END, lines)
        self.data = map(PositionData.parse_line, lines)

    def __enter__(self) -> PositionData:
        return self

    def __exit__(self, *_) -> None:
        self.file.close()

    def __iter__(self) -> PositionData:
        return self

    def __next__(self) -> tuple[float, Vector, Vector]:
        return next(self.data)


if __name__ == "__main__":
    name = input("Enter celestial: ")
    data = PositionData(name)
    for time, pos, vel in data:
        if input(f"time={time} pos={pos} vel={vel} ").lower() == "q":
            break
