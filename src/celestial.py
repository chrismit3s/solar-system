from __future__ import annotations
from src.data import PositionData, MASS
from src import Vector
from typing import Iterable, Callable
import numpy as np

G = 6.67e-11  # m**3/kg*s**2


class Celestial:
    @staticmethod
    def from_name(name: str) -> Celestial:
        if name not in MASS:
            raise ValueError(f"No mass for celestial {name}")
        mass = MASS[name]
        _, pos, vel = next(PositionData(name))
        return Celestial(name, pos, vel, mass)

    @staticmethod
    def acc_newton(pos: Vector, celestials: Iterable[Celestial]) -> Vector:
        a: Vector = np.zeros(3)
        for planet in celestials:
            d = planet.pos - pos
            r = np.linalg.norm(d)
            a += G * planet.mass * d / r**3
        return a

    @staticmethod
    def acc_einstein(pos: Vector, celestials: Iterable[Celestial]) -> Vector:
        return np.zeros(3)

    def __init__(self, name: str, pos: Vector, vel: Vector, mass: float):
        self.name: str = name
        self.mass: float = mass
        self.pos_hist: list[Vector] = [pos]
        self.vel_hist: list[Vector] = [vel]

    @property
    def pos(self) -> Vector:
        return self.pos_hist[-1]

    @property
    def vel(self) -> Vector:
        return self.vel_hist[-1]

    def update(self, pos: Vector, vel: Vector) -> None:
        self.pos_hist.append(pos)
        self.vel_hist.append(vel)

    def update_newton(self, dt: float, compute_acc: Callable[[Vector], Vector]) -> None:
        acc = compute_acc(self.pos)
        vel = acc * dt + self.vel
        pos = 1 / 2 * acc * dt**2 + self.vel * dt + self.pos
        self.update(pos, vel)

    def update_leapfrog(self, dt: float, compute_acc: Callable[[Vector], Vector]) -> None:
        pos_intermediate = self.pos + self.vel * dt / 2
        acc_intermediate = compute_acc(pos_intermediate)

        vel = self.vel + acc_intermediate * dt
        pos = pos_intermediate + vel * dt / 2
        self.update(pos, vel)

    def update_runge_kutta(self, dt: float, compute_acc: Callable[[Vector], Vector]):
        pos0: Vector = self.pos
        acc0: Vector = compute_acc(pos0)
        vel0: Vector = self.vel

        pos1: Vector = pos0 + vel0 * dt / 2
        acc1: Vector = compute_acc(pos1)
        vel1: Vector = vel0 + acc1 * dt / 2

        pos2: Vector = pos0 + vel1 * dt / 2
        acc2: Vector = compute_acc(pos2)
        vel2: Vector = vel0 + acc2 * dt / 2

        pos3: Vector = pos0 + vel2 * dt
        acc3: Vector = compute_acc(pos3)
        vel3: Vector = vel0 + acc3 * dt

        pos = pos0 + np.mean([vel0, 2 * vel1, 2 * vel2, vel3], axis=0) * dt  # type: ignore
        vel = vel0 + np.mean([acc0, 2 * acc1, 2 * acc2, acc3], axis=0) * dt  # type: ignore
        self.update(pos, vel)


if __name__ == "__main__":
    pass
